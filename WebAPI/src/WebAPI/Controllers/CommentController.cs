﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;
using Microsoft.AspNetCore.Authorization;

namespace TodoApi.Controllers {

    [Route("api/[controller]")]
    public class CommentController : Controller {

        public ICommentRepository Comments { get; set; }
        public CommentController(ICommentRepository icr) {
            Comments = icr;
        }

        [HttpGet]
        public IEnumerable<Comment> GetAll(){

            return Comments.GetAll();
        }

        [HttpGet("{id}", Name = "GetComment")]
        public IActionResult GetById(string id) {

            var c = Comments.Get(id);
            if (c == null) {
                return new NotFoundResult();
            }
            return new ObjectResult(c);
        }

        [Authorize]
        [HttpPost]
        public IActionResult Post([FromBody] Comment c) {

            if(c == null) {
                return BadRequest();
            }

            Comments.Add(c);
            return CreatedAtRoute("GetComment", new { id = c.id }, c);
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] Comment c) {

            if (c == null || c.id != id) {
                return BadRequest();
            }

            var comm = Comments.Get(id);
            if (comm == null) {
                return NotFound();
            }

            Comments.Update(c);
            return new NoContentResult();
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(string id) {

            var comm = Comments.Get(id);
            if(comm == null) {
                return new NotFoundResult();
            }

            Comments.Remove(id);
            return new NoContentResult();
        }


    }
}