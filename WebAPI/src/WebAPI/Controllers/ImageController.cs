﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;

namespace WebAPI.Controllers {
    [Route("api/[controller]")]
    public class ImageController : Controller {

        public IimageRepo Images { get; set; }
        public ImageController(IimageRepo iir) {
            Images = iir;
        }

       [HttpGet]
        public IEnumerable<Image> GetAll() {

            return Images.GetAll();
        }

        [HttpGet("{id}", Name = "GetImage")]
        public IActionResult Get(string id) {

            var img = Images.Get(id);
            if (img == null) {
                return NotFound();
            }

            return new ObjectResult(img);
        }
        


        //[Authorize]
        [HttpPost]
        public IActionResult Post(Image img, IFormFile file) {

            if (file == null) {
                return BadRequest();
            }

            Images.Add(img, file);

            return Ok();
           // return new ObjectResult(img);
        }

        [Authorize]
        [HttpPut("{id}")]
        public IActionResult Update([FromBody] Image img) {


            if (img.id == null ) {
                return BadRequest();
            }

            var i = Images.Get(img.id);
            if (i == null) {
                return NotFound();
            }

            Images.Update(img);
            return new NoContentResult();
        }

        [Authorize]
        [HttpDelete("{id}")]
        public IActionResult Delete(string id) {

            var img = Images.Get(id);
            if (img == null) {

                return NotFound();
            }

            Images.Remove(id);
            return new NoContentResult();
        }


    }
  
}

