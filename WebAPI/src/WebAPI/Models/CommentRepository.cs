﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Diagnostics;
using System.Linq;

namespace WebAPI.Models {
    public class CommentRepository : ICommentRepository {

        public void Add(Comment c) {
            c.id = Guid.NewGuid().ToString();
            c.time = DateTime.Now;

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "INSERT into dbo.Comment VALUES (@id, @time, @userId, @imageId, @text)";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString,
                    new { id = c.id, time = c.time, userId = c.userId, imageId = c.imageId, text = c.text });
                                
                sqlConnection.Close();
            }
         }

        public Comment Get(string id) {

            Comment comment;

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.Comment WHERE id=(@ID)";
                sqlConnection.Open();
                comment = (Comment) sqlConnection.Query<Comment>(sqlString, new { ID = id }).SingleOrDefault(); ;
                sqlConnection.Close();
            }
            return comment;
        }

        public IEnumerable<Comment> GetAll() {

            List<Comment> comments = new List<Comment>();

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.Comment";
                sqlConnection.Open();
                comments = (List<Comment>) sqlConnection.Query<Comment>(sqlString);
                sqlConnection.Close();
            }
            return comments;
        }

        public Comment Remove(string ID) {

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "DELETE FROM dbo.Comment WHERE id=(@id)";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString, new { id = ID });
                sqlConnection.Close();
            }

            return null;
        }

        public void Update(Comment c) {

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "UPDATE Comment SET text = @text WHERE id = @id";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString, new { id = c.id, text = c.text });
                sqlConnection.Close();
            }

        }

    }
} 