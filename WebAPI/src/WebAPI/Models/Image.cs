﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Image
    {
        public string id { get; set; }
        public DateTime time { get; set; }
        public string filePath { get; set; }
        public string userId { get; set; }
        public string fileName { get; set; }

        public string title { get; set; }
        public string imageComment { get; set; }
    }
}
