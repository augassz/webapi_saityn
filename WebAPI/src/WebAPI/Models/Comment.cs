﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Comment
    {
        public string id { get; set; }
        public DateTime time { get; set; }
        public string userId { get; set; }
        public string imageId { get; set; }

        public string text { get; set; }
    }
}
