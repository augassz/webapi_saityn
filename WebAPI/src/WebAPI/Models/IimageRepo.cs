﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace WebAPI.Models {
    public interface IimageRepo
    {
        void Add(Image img, IFormFile file);
        IEnumerable<Image> GetAll();
        Image Get(string id);
        void Update(Image img);
        Image Remove(string id);
    }
}
