﻿using Dapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace WebAPI.Models {
    public class ImageRepo : IimageRepo {

        const string uploadPath = "..\\..\\..\\Angular\\angular-webpack\\Images\\";
        const string trueFilePath = "../../../Images/";

        public void Add(Image img, IFormFile file) {


            Debug.WriteLine(img.imageComment);
            Debug.WriteLine(img.title);
            Debug.WriteLine(img.userId);
            Debug.WriteLine(file.FileName);


            string ext = Path.GetExtension(file.FileName);
            if (ext == ".jpg" || ext == ".jpeg" || ext == ".png" || ext == ".PNG" || ext == ".gif") { }
            else
                throw new Exception("File is not an image or bad image extension, needs to be .jpg |.png |.gif");
            
            img.id = Guid.NewGuid().ToString();
            img.time = DateTime.Now;
            img.fileName = img.time.ToString().Replace("-", "").Replace(":", "").Replace(" ", "") + file.FileName;
            img.filePath = Path.Combine(trueFilePath, img.fileName);

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "INSERT into dbo.Image VALUES (@id, @time, @filePath, @userId, @fileName, @title, @imageComment)";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString,
                    new { id = img.id, time = img.time, filePath = img.filePath, userId = img.userId, fileName = img.fileName, title = img.title, imageComment = img.imageComment });

                sqlConnection.Close();
            }

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);

            try {
                using (FileStream fs = new FileStream(uploadPath + img.fileName, FileMode.Create)) {
                    file.CopyTo(fs);
                }
            }
            catch (DirectoryNotFoundException dirEx) {
                Console.WriteLine("Directory not found: " + dirEx.Message);
            }
            
        }

        public Image Get(string ID) {
            Image img;

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.Image WHERE id=(@id)";
                sqlConnection.Open();
                img = (Image)sqlConnection.Query<Image>(sqlString, new { id = ID }).SingleOrDefault(); ;
                sqlConnection.Close();
            }

            return img;
        }

        public IEnumerable<Image> GetAll() {
            List<Image> imgList = new List<Image>();

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.Image";
                sqlConnection.Open();
                imgList = (List<Image>)sqlConnection.Query<Image>(sqlString);
                sqlConnection.Close();
            }

            return imgList;
        }

        public Image Remove(string ID) {

            string fPath = null;

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT filePath FROM dbo.Image WHERE id=(@id)";
                sqlConnection.Open();
                Image img = (Image)sqlConnection.Query<Image>(sqlString, new { id = ID }).SingleOrDefault();
                sqlConnection.Close();
                fPath = img.filePath;
            }

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "DELETE FROM dbo.Image WHERE id=(@id)";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString, new { id = ID });
                sqlConnection.Close();
            }

            if(File.Exists(fPath))
                File.Delete(fPath);

            return null;
        }

        public void Update(Image img) {

            Debug.WriteLine(img.id);
            Debug.WriteLine(img.id);
            Debug.WriteLine(img.id);



            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "UPDATE Image SET title = @title, imageComment = @imgComment  WHERE id = @id";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString, new { id = img.id, title = img.title, imgComment = img.imageComment });
                sqlConnection.Close();
            }
        }








    }
}

