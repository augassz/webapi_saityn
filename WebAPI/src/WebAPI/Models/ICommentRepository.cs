﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public interface ICommentRepository
    {
        void Add(Comment c);
        IEnumerable<Comment> GetAll();
        Comment Get(string key);
        void Update(Comment c);
        Comment Remove(string key);
    }
}
