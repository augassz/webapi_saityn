﻿using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Web_Api_users.Controllers;
using Web_Api_users.Models;

namespace Web_Api_users {
    public class Startup
    {
        public static string DBConnectionString { get; private set; }
        private readonly IHostingEnvironment _environment;


        public Startup(IHostingEnvironment env)
        {
            _environment = env;
            DBConnectionString = "Server=(localdb)\\mssqllocaldb;Database=WebAPI_DB;Trusted_Connection=True;MultipleActiveResultSets=true";

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddMvc();
            services.AddSingleton<IuserRepo, UserRep>();

            services.AddCors(options => {
                options.AddPolicy("AllowSpecificOrigin",
                builder => builder.WithOrigins("http://localhost:8080")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                );
            });

            var contentRootPath = _environment.ContentRootPath;

            var cert = new X509Certificate2(Path.Combine(contentRootPath, "idsrv3test.pfx"), "idsrv3test");


            services.AddIdentityServer()
                .AddSigningCredential(cert)
                .AddInMemoryScopes(Configurations.Scopes.GetScopes())
                .AddInMemoryClients(Configurations.Clients.GetClients());

            services.AddTransient<IResourceOwnerPasswordValidator, Configurations.ResourceOwnerPasswordValidator>();
            services.AddTransient<IProfileService, Configurations.ProfileService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Debug);
            app.UseDeveloperExceptionPage();
            app.UseCors("AllowSpecificOrigin");

            app.UseIdentityServer();
            app.UseMvc();
        }
    }
}
