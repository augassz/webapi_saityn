﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api_users.Models
{
    public interface IuserRepo
    {
        void Add(User u);
        IEnumerable<User> GetAll();
        User Get(string id);
        string Get(string email, string password);
        void Update(User u);
        User Remove(string id);
    }
}
