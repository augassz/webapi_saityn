﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api_users.Models
{
    public class User
    {
        public string id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
