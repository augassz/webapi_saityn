﻿using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;

namespace Web_Api_users.Models {
    public class UserRep : IuserRepo {


        public void Add(User u) {
            u.id = Guid.NewGuid().ToString();

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "INSERT into dbo.[User] VALUES (@id, @email, @password)";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString,
                    new { id = u.id, email = u.email, password = u.password });

                sqlConnection.Close();
            }

        }

        public IEnumerable<User> GetAll() {

            List<User> users = new List<User>();

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.[User]";
                sqlConnection.Open();
                users = (List<User>)sqlConnection.Query<User>(sqlString);
                sqlConnection.Close();
            }
            return users;
        }

        public User Get(string id) {
            User usr = null;

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.[User] WHERE id=(@id)";
                sqlConnection.Open();
                usr = (User)sqlConnection.Query<User>(sqlString, new { ID = id }).SingleOrDefault(); ;
                sqlConnection.Close();
            }

            return usr;
        }

        public string Get(string email, string password){
            string idStr = "";
            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "SELECT * FROM dbo.[User] WHERE email=(@em) and password=(@pass)";
                sqlConnection.Open();
                var record = (User)sqlConnection.Query<User>(sqlString, new { em = email, pass = password }).FirstOrDefault(); ;
                sqlConnection.Close();
                if (record != null)
                    idStr = record.id;
            }
            return idStr;
        }

       
        public User Remove(string ID) {

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "DELETE FROM dbo.[User] WHERE id=(@id)";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString, new { id = ID });
                sqlConnection.Close();
            }

            return null;
        }

        public void Update(User u) {

            using (var sqlConnection = new SqlConnection(Startup.DBConnectionString)) {

                string sqlString = "UPDATE [User] SET email = @email, password = @password WHERE id = @id";
                sqlConnection.Open();
                int result = sqlConnection.Execute(sqlString, new { id = u.id, email = u.email, password = u.password });
                sqlConnection.Close();
            }

        }
    }
}
