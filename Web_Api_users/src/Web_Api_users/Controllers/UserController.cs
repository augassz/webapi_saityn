﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Web_Api_users.Models;

namespace Web_Api_users.Controllers {

    [Route("api/[controller]")]
    public class UserController : Controller
    {
        public IuserRepo users { get; set; }
        public UserController (IuserRepo iur) {
            users = iur;
        }

        [HttpGet]
        public IEnumerable<User> GetAll() {
            return users.GetAll();
        }

        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult GetById(string id) {

            var u = users.Get(id);
            if (u == null) {
                return new NotFoundResult();
            }
            return new ObjectResult(u);
        }

        [HttpPost]
        public IActionResult Post([FromBody] User u) {

            if (u == null) {
                return BadRequest();
            }

            users.Add(u);
            return CreatedAtRoute("GetUser", new { id = u.id }, u);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] User u) {

            if (u == null || u.id != id) {
                return BadRequest();
            }

            var usr = users.Get(id);
            if (usr == null) {
                return NotFound();
            }

            users.Update(u);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id) {

            var usr = users.Get(id);
            if (usr == null) {
                return new NotFoundResult();
            }

            users.Remove(id);
            return new NoContentResult();
        }
    }
}
