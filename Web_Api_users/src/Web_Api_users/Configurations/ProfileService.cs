﻿using IdentityServer4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using System.Diagnostics;

namespace Web_Api_users.Configurations
{
    public class ProfileService : IProfileService {

        public Task GetProfileDataAsync(ProfileDataRequestContext context) {

            context.IssuedClaims = context.Subject.Claims.ToList(); // claims filter
            return Task.FromResult(0);

        }

        public Task IsActiveAsync(IsActiveContext context) {

            return Task.FromResult(0);

        }

    }
}

