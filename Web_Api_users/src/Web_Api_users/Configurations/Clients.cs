﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api_users.Configurations
{
    public class Clients
    {

        public static List<Client> GetClients() {

            return new List<Client>() {

                new Client() {
                    ClientId = "Client",
                    ClientSecrets = new List<Secret>() {
                        new Secret("topsecret".Sha256())
                    },
                    AllowedScopes = new List<string>() {
                        "MyAPI", "api1"
                    },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword

                },

                new Client() {
                    ClientId = "ServerClient",
                    ClientSecrets = new List<Secret>() {
                        new Secret("serverSecret".Sha256())
                    },
                    AllowedScopes = new List<string>() {
                        "MyAPI", "api1"
                    },
                    AllowedGrantTypes = GrantTypes.ClientCredentials

                }

            };

        }
    }
}
