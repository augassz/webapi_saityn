﻿using Dapper;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Web_Api_users.Models;
using static IdentityModel.OidcConstants;

namespace Web_Api_users.Configurations
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator {

        IuserRepo userRepo = new UserRep();

        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context) {

            string userId = userRepo.Get(context.UserName, context.Password);

            if (string.IsNullOrWhiteSpace(userId)) {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, "User email or password is incorect.");
                return Task.FromResult(0);
            }
            else {
                context.Result = new GrantValidationResult(userId, "password");
                return Task.FromResult(0);
            }

        }
        

    }
}
