﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_Api_users.Configurations
{
    public class Scopes
    {

        public static List<Scope> GetScopes() {

            return new List<Scope>() {
                new Scope {
                    Name = "MyAPI",
                    Description = "MyAPI description."
                },
                new Scope {
                    Name = "api1",
                    Description = "api1 Image and comments."
                }
            };
        }

    }
}
