import {Component, OnInit} from '@angular/core';
import { UserService } from '../Users/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'register',
  template: `
    <div style="text-align: center">
    <h1>
      {{ title }}
      <hr>
    </h1>
      
      <form class="form-inline">
        <input #email type="email" placeholder="E-mail"><br><br>
        <input #password1 type="password"  placeholder="Password"><br><br>
        <input #password2 type="password"  placeholder="Repeat password"><br><br>
        <button type="submit" class="btn" (click)="register(email.value,password1.value,password2.value)">Register</button>
      </form>
        <router-outlet></router-outlet>
      <h2 class="text-error"> {{ message }}</h2>

    <hr>
    </div>
  `
})
export class RegisterComponent implements OnInit {
  title: string = 'Register Page';
  message: string;

    constructor( private userService: UserService, private router: Router) {   }

  ngOnInit() {
  }

  updateMessage(m: string): void {
    this.message = m;
  }

  check(aaaa : any) : void{
    if(aaaa == null){
        this.message = "Registration was not successful.";
     }
     else{
         this.router.navigate(['/login']);
     }
    }

  register(email : string, password1 : string, password2 : string){

    if(password1 == "" || password2 == ""){
      this.message = "Password cant be empty.";
      return;
    }

    if(password1.length < 6){
      this.message = "Password too short.";
      return;
    }

    if(password1 != password2){
      this.message = "Passwords dont match.";
      return;
    }

    this.userService.postUser(email, password1).then(res =>{
                res,
                this.check(res);
    } );
  }


}