import {Component, OnInit} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import 'rxjs/Rx'; 


import { Image } from '../Images/image';
import { ImageService } from '../Images/image.service';
import { ServerAuthService } from '../../app/server.auth.service';


@Component({
  selector: 'home',
  template: require('./home.component.html')
})

export class HomeComponent implements OnInit {
  title: string = 'Home Page';
  body:  string = '';
  images: Image[];

  showImage: false;
  selectedImage: Image;

  constructor(private imageService: ImageService, private router: Router) {   }

  ngOnInit() {
    this.getImages();
  }

  getImages(): void {
    this.imageService.getImages().then(images =>{
      this.images = images;
      this.images.sort(function(a:Image, b:Image){
              if(a.time > b.time) return -1;
              if(a.time < b.time) return 1;
              return 0;
              });
    })
  }
  
  onSelect(img: Image): void {
    this.selectedImage = img;
    window.open('/image/'+ this.selectedImage.id.toString(), '', '', false);
    //this.router.navigate(['/image', this.selectedImage.id]);
  
  }
 

}