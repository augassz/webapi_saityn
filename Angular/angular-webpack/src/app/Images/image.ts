export class Image {
  id: string;
  time: string;
  filePath: string;
  userId: string;
  fileName: string;
  title: string;
  imageComment: string;
}