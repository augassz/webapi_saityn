import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Image } from './image';
import { Observable } from 'rxjs';



@Injectable()
export class ImageService {

    private imagesUrl = 'http://localhost:5001/api/image';  // URL to web api

    constructor(private http: Http) { }


    getImages(): Promise<Image[]> {

       // var token = JSON.parse(localStorage.getItem('currentUser'));              // apsaugotiemsĄĄĄ!!!!! svarbu
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
       // headers.append('Authorization', 'Bearer ' + token.token)                  // apsaugotiemsĄĄĄ!!!!! svarbu

        return this.http.get(this.imagesUrl, { headers : headers })
                .toPromise()
                .then(response =>{
                    var images = response.json() as Image[];
                    return images;
                } ).catch(this.handleError);
    }
    
    uploadImage(title: string, imageComment : string, file : any) : Observable<boolean> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        var token = JSON.parse(localStorage.getItem('currentUser'));            
        headers.append('Authorization', 'Bearer ' + token.token);
        let uID = token.id;

        let input = new FormData();
        input.append("title", title);
        input.append("imageComment", imageComment);
        input.append("userId", uID);
        input.append("file", file);

        return this.http.post(this.imagesUrl, input, { headers: headers })
                .map((response: Response) => {

                    console.log(response.toString());
                let token = response.json() && response.json().access_token;

                if (token) {


                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
        }
    


    /* 

        var body = "grant_type=" + grant_type + "&client_id=" + client_id + "&client_secret=" + client_secret + "&username=" + username + "&password=" + password;


        return this.http.post('http://localhost:5000/connect/token', body, { headers : headers })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().access_token;

                if (token) {
                    // set token property
                    this.token = token;

                    this.userService.getUserIdByName(username).then(result =>{
                        localStorage.setItem('currentUser', JSON.stringify({ id : result.toString(), username: username, token: token }));
                    } );

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    };*/

    deleteImage(id: string) : Promise<Image> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var token = JSON.parse(localStorage.getItem('currentUser'));            
        headers.append('Authorization', 'Bearer ' + token.token);
        var body = JSON.stringify({id : id});

        return this.http.delete(this.imagesUrl + "/" + id.toString(), { headers: headers })
                .toPromise()
                .then(() => null)
                .catch(this.handleError);
        }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getImage(id: string): Promise<Image> {
    return this.getImages().then(images => images.find(image => image.id === id));
   }

}