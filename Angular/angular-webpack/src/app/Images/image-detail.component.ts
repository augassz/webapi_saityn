import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { Image } from './image';
import { ImageService }  from './image.service';
import { Comment } from '../Comments/comment';
import { CommentService } from '../Comments/comment.service';
import { UserService } from '../Users/user.service';
import { User } from '../Users/user';

@Component({
  selector: 'image-detail',
  template: `
    <div style="text-align: center" *ngIf="image">
    <h1> {{ image.title }}</h1>
    <hr>
    <img style="border:1px dashed DarkSlateBlue;" src="{{image.filePath}}" alt="{{image.title}}">
    <div>Author: <b>{{getUserName(image.userId)}}</b><label>{{image.imageComment}}  </label></div><br>
    <div style="display: inline-block;" *ngIf="checkUser(image.userId)"> <button type="submit" (click)="deleteImage(image.id)">x</button> </div>
    <div style="display: inline-block;" ><label>{{image.time}}  </label></div><br>
    </div>
    <div style="text-align: left">
        <form class="form-inline">
            <input #msg placeholder="Message text"/>&nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn" (click)="sendComment(msg.value)">Send</button>
        </form> 
    </div>
    <hr>
    <div style="text-align: left" *ngIf="comments">
    <label *ngFor="let comment of comments">
            <div style="display: inline-block;" *ngIf="checkUser(comment.userId)"> <button type="submit" (click)="deleteComment(comment.id)">x</button> </div>
            <div style="display: inline-block;"> {{comment.time}}: &nbsp;<b>{{getUserName(comment.userId)}}</b>&nbsp; <b>:</b> &nbsp;&nbsp;{{comment.text}} </div>
    </label>
    </div>
  `
})
export class ImageDetailComponent implements OnInit {
  image: Image;
  comments: Comment[];
  usersMap = new Map<string, string>();
  check:boolean = true;

  constructor(
    private imageService: ImageService,
    private commentService: CommentService,
    private userService : UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
      this.route.params.switchMap((params: Params) =>
      this.imageService.getImage(params['id'])).subscribe(image => this.image = image);

      this.loadComments();

  }

  loadComments(){
      this.route.params.switchMap((params: Params) =>
      this.commentService.getCommentsByImageId(params['id'])).subscribe(comments =>{
            this.comments = comments;
            this.comments.sort(function(a:Comment, b:Comment){
              if(a.time > b.time) return -1;
              if(a.time < b.time) return 1;
              return 0;
              });
              comments.forEach(element => {
                  this.userService.getUserById(element.userId).then(res =>{
                    res,
                    this.usersMap.set(element.userId, res.toString())
                  } );
              });

      } );


  }

  checkUser(usrId: string) : boolean{
           if(JSON.parse(localStorage.getItem('currentUser')) != null){
            if(usrId == JSON.parse(localStorage.getItem('currentUser')).id)
                return true;
            else
                return false;
           }
  }

  getUserName(id : string) : string{
      return this.usersMap.get(id);
  }

  sendComment(msg: string): void{

        if(msg == "")
            return;

        if(localStorage.getItem('currentUser') == null){
            this.router.navigate(['/login']);
        }
        else{
            this.commentService.postComment(JSON.parse(localStorage.getItem('currentUser')).id, this.image.id, msg).then(res =>{
                res,
                this.loadComments();
                } );
        }

  }

  deleteComment(id: string): void{

      this.commentService.deleteComment(id).then(res =>{
                res,
                this.loadComments();
      } );

  }

  deleteImage(id: string) : void{
      this.imageService.deleteImage(id).then(res =>{
                res,
                this.router.navigate(['/home']);
      } );
  }

}
