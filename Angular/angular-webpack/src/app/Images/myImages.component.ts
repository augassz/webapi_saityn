import {Component, OnInit} from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import 'rxjs/Rx'; 


import { Image } from '../Images/image';
import { ImageService } from '../Images/image.service';
import { ServerAuthService } from '../../app/server.auth.service';


@Component({
  selector: 'home',
  template: `
  <div>
<div style="text-align: center; ">
<h1>

  {{ title }}
</h1>
</div>
<hr>

<div  > 
 <ul >
  <li style="display: inline-block;" *ngFor="let image of images ">
  <div *ngIf="checkUser(image.userId)">
       <b>  {{image.title}} </b>
     <br>
     <img style="border:1px dashed DarkSlateBlue;"  src="{{image.filePath}}" alt="{{image.title}}" height="200" width="200" (click)="onSelect(image)">
     <br><br><br><br>
</div>
  </li>
</ul> 
</div>


    <router-outlet></router-outlet>
</div>
`
})
//
export class MyImagesComponent implements OnInit {
  title: string = 'My Uploads';
  body:  string = '';
  images: Image[];//

  showImage: false;
  selectedImage: Image;

  constructor( private imageService: ImageService, private router: Router) {   }

  ngOnInit() {
    this.getImages();
  }

  getImages(): void {
    this.imageService.getImages().then(images =>{
      this.images = images;
      this.images.sort(function(a:Image, b:Image){
              if(a.time > b.time) return -1;
              if(a.time < b.time) return 1;
              return 0;
              });
    })
  }
  
  onSelect(img: Image): void {
    this.selectedImage = img;
    window.open('/image/'+ this.selectedImage.id.toString(), '', '', false);
  
  }

  checkUser(usrId: string) : boolean{
           if(JSON.parse(localStorage.getItem('currentUser')) != null){
            if(usrId == JSON.parse(localStorage.getItem('currentUser')).id)
                return true;
            else
                return false;
           }
  }
 

}