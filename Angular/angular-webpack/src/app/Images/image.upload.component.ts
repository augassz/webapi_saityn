import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { Image } from './image';
import { ImageService }  from './image.service';
import { Comment } from '../Comments/comment';
import { CommentService } from '../Comments/comment.service';
import { UserService } from '../Users/user.service';
import { User } from '../Users/user';

@Component({
  selector: 'image-upload',
  template: `
    <div style="text-align: center">
     <h1>    Upload your image </h1>
<hr>
  <form class="form-inline">
    <input #title type="text"  placeholder="Image Title"><br><br>
    <input #imageComment type="text"  placeholder="Image Comment"><br><br>
    <input #imageFile type="file" />
    <br><br><button type="submit" class="btn" (click)="uploadImage(title.value,imageComment.value)">Upload</button>
  </form>
    <router-outlet></router-outlet>
  <h2 class="text-error"> {{ message }}</h2>
  <hr>

  </div>
  `
})
export class ImageUploadComponent implements OnInit {
    title: string = "Upload your image"
  image: Image;
  comments: Comment[];
  usersMap = new Map<string, string>();
  check:boolean = true;

  constructor(
    private imageService: ImageService,
    private commentService: CommentService,
    private userService : UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void { }

    @ViewChild("imageFile") imageFile : ElementRef;

    uploadImage(title:string, imgComment: string){

            if(title == "")
                return;
            else if(imgComment == "")
                return;

            let fileToUpload = this.imageFile.nativeElement.files[0];
            if(fileToUpload){
             //   this.imageService.uploadImage()
             this.imageService.uploadImage(title, imgComment, fileToUpload)
                .subscribe(result => {
                    if (result === true) {
                        // login successful
                       // this.router.navigate(['/home']);
                    } else {
                        // login failed
                       // this.message = 'Error: Username or password is incorrect';
                    }
            });
                
      
            }
    }


}
