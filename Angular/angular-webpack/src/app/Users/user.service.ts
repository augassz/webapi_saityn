import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class UserService {

    private usersUrl = 'http://localhost:5000/api/user';  // URL to web api

    constructor(private http: Http) { }

    getUsers(): Promise<User[]> {
    return this.http.get(this.usersUrl)
               .toPromise()
               .then(response =>{
                   var users = response.json() as User[];
                   return users;
               } ).catch(this.handleError);
    }

   postUser(email : string, password : string): Promise<User> {
    let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var body = JSON.stringify({email : email, password : password});

        return this.http.post(this.usersUrl, body, { headers: headers })
                    .toPromise()
                    .then(res => res.json())
                    .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getUserById(id: string): Promise<string>  {
        return this.getUsers().then(users => users.filter(user => user.id === id)[0].email);
    }

    getUserIdByName(name: string): Promise<string>  {
        return this.getUsers().then(users => users.filter(user => user.email === name)[0].id);
    }

}