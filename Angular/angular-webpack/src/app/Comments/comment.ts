export class Comment {
  id: string;
  time: string;
  userId: string;
  imageId: string;
  text: string;
}