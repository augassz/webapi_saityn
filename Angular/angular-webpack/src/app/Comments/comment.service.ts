import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Comment } from './comment';



@Injectable()
export class CommentService {

    private commentsUrl = 'http://localhost:5001/api/comment';  // URL to web api

    constructor(private http: Http) { }

    getComments(): Promise<Comment[]> {
    return this.http.get(this.commentsUrl)
               .toPromise()
               .then(response =>{
                   var comments = response.json() as Comment[];
                   return comments;
               } ).catch(this.handleError);
    }

    postComment(id: string, imgId: string,  msg : string) : Promise<Comment> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var token = JSON.parse(localStorage.getItem('currentUser'));            
        headers.append('Authorization', 'Bearer ' + token.token);
        var body = JSON.stringify({userId : id, imageId : imgId, text : msg});

        return this.http.post(this.commentsUrl, body, { headers: headers })
                    .toPromise()
                    .then(res => res.json())
                    .catch(this.handleError);
    } 


    deleteComment(id: string) : Promise<Comment> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var token = JSON.parse(localStorage.getItem('currentUser'));            
        headers.append('Authorization', 'Bearer ' + token.token);
        var body = JSON.stringify({id : id});

        return this.http.delete(this.commentsUrl + "/" + id.toString(), { headers: headers })
                .toPromise()
                .then(() => null)
                .catch(this.handleError);
        }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    getCommentsByImageId(id: string): Promise<Comment[]>  {
        return this.getComments().then(comments => comments.filter(comment => comment.imageId === id));

  }

}