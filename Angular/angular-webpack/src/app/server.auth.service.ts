import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

@Injectable()
export class ServerAuthService {
    public token: string;

    constructor(private http: Http) {
        // set token if saved in local storage
        var serverToken = JSON.parse(localStorage.getItem('serverToken'));
        this.token = serverToken && serverToken.token;
    }

    loginToServer(): Observable<boolean> {

        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let grant_type = 'client_credentials';
        let client_id = 'ServerClient';
        let client_secret = 'serverSecret';
        var body = "grant_type=" + grant_type + "&client_id=" + client_id + "&client_secret=" + client_secret;

        return this.http.post('http://localhost:5000/connect/token', body, { headers : headers })
            .map((response: Response) => {
                let token = response.json() && response.json().access_token;

                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('serverToken', JSON.stringify({ token: token }));
                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

}