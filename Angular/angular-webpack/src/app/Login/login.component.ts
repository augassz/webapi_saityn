import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'login',
  template:`
      <div style="text-align: center">
  <h1>
    {{ title }}
    <hr>
  </h1>

  <form class="form-inline">
    <input #email type="text"  placeholder="E-mail"><br><br>
    <input #pass type="password"  placeholder="Password"><br><br>
    <button type="submit" class="btn" (click)="login(email.value,pass.value)">Login</button>
  </form>
    <router-outlet></router-outlet>
  <h2 class="text-error"> {{ message }}</h2>
  <hr>
  </div>
  `
})
export class LoginComponent  {
  title: string = 'Login Page';
  message: string;

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  login(email : string, password : string) {

        this.authenticationService.login(email, password)
            .subscribe(result => {
                if (result == true) {
                    // login successful
                    this.router.navigate(['/home']);
                } else {
                    // login failed
                    this.message = 'Error: Username or password is incorrect';
                }
            });

  }

  
}