import { Component } from '@angular/core';
import { Router, Routes, RouterModule} from '@angular/router';

import { HomeComponent } from './Home/home.component'
import { LoginComponent } from './Login/login.component'
import { RegisterComponent } from './Register/register.component'
import { AuthenticationService } from './authentication.service'


@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent { 
    
  constructor(private authService : AuthenticationService, private router: Router){}

    checkIfLoggedIn() : boolean{
           if(JSON.parse(localStorage.getItem('currentUser')) != null){
                return true;
           }
           else{
                return false;
           }        
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/home']);
  }

}
