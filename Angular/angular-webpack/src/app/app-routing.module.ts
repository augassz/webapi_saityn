import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './Home/home.component'
import { LoginComponent } from './Login/login.component'
import { RegisterComponent } from './Register/register.component'
import { ImageService } from './Images/image.service';
import { ImageDetailComponent } from './Images/image-detail.component'
import { ImageUploadComponent } from './Images/image.upload.component'
import { MyImagesComponent } from './Images/myImages.component'



const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home',  component: HomeComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'register',  component: RegisterComponent },
  { path: 'imageUpload', component: ImageUploadComponent },
  { path: 'myImages', component: MyImagesComponent },
  { path: 'image/:id', component: ImageDetailComponent }
  //{ path: "**", redirectTo: '/home' }

 // { path: 'heroes',     component: HeroesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}