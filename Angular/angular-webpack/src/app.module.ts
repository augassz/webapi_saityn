import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { HttpModule }    from '@angular/http';


import { AppComponent } from './app/app.component';
import { HomeComponent } from './app/Home/home.component'
import { LoginComponent } from './app/Login/login.component'
import { RegisterComponent } from './app/Register/register.component'
import { ImageService } from './app/Images/image.service';
import { CommentService } from './app/Comments/comment.service';
import { UserService } from './app/Users/user.service';
import { ImageDetailComponent } from './app/Images/image-detail.component'
import { ImageUploadComponent } from './app/Images/image.upload.component'
import { MyImagesComponent } from './app/Images/myImages.component'
import { AuthenticationService } from './app/authentication.service'
import { ServerAuthService } from './app/server.auth.service'

import { AppRoutingModule } from './app/app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ImageDetailComponent,
    ImageUploadComponent,
    MyImagesComponent
  ],
  providers: [
    ImageService,
    CommentService,
    UserService,
    AuthenticationService,
    ServerAuthService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
